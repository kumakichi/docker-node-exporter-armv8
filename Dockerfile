# alpine repo/image is multiarch or architecture-cognitive so
# no specific arch-flags /paths needed
FROM alpine:3.16.1
ARG RELEASE="1.3.1"

RUN apk --update --no-cache add ca-certificates curl wget \
    && mkdir -p /tmp/install /bin \
    && cd /tmp/install \
    && wget -qO- https://github.com/prometheus/node_exporter/releases/download/v$RELEASE/node_exporter-$RELEASE.linux-arm64.tar.gz |tar xz \
    && cp /tmp/install/node_exporter-$RELEASE.linux-arm64/node_exporter /bin 

ENV VERSION 1.3.1 

EXPOSE     9100
ENTRYPOINT [ "/bin/node_exporter" ]

